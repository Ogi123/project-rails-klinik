class CreateBuatjanjis < ActiveRecord::Migration[7.0]
  def change
    create_table :buatjanjis do |t|
      t.integer :pasien_id
      t.integer :dokter_id
      t.integer :poliklinik_id
      t.integer :jadwalpraktek_id
      t.date    :tgl_janji
      t.timestamps
    end
  end
end
