class CreatePasiens < ActiveRecord::Migration[7.0]
  def change
    create_table :pasiens do |t|
      t.string  :nama
      t.string  :no_tlp
      t.string  :alamat
      t.date    :tgl_lahir
      t.integer :jk
      t.string  :tinggi_badan
      t.string  :berat_badan
      
      t.timestamps
    end
  end
end