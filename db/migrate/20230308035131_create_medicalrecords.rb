class CreateMedicalrecords < ActiveRecord::Migration[7.0]
  def change
    create_table :medicalrecords do |t|
        t.integer :pasien_id
        t.integer :dokter_id
        t.integer :buatjanji_id
        t.date    :tgl_record
        t.string  :diagnosa
        t.timestamps
      end
    end
  end
  