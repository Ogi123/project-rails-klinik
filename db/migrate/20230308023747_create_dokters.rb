class CreateDokters < ActiveRecord::Migration[7.0]
  def change
    create_table :dokters do |t|
      t.integer :poliklinik_id
      t.string  :nama
      t.string  :email
      t.string  :no_tlp
      t.integer :jk
      t.string  :alamat
      t.string  :spesialis
      t.string  :password_digest
      t.timestamps
    end
  end
end
