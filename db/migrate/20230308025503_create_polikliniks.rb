class CreatePolikliniks < ActiveRecord::Migration[7.0]
  def change
    create_table :polikliniks do |t|
      t.string  :nama_poli
      t.timestamps
    end
  end
end
