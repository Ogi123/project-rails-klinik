class CreateJadwalprakteks < ActiveRecord::Migration[7.0]
  def change
    create_table :jadwalprakteks do |t|
      t.integer :dokter_id
      t.date    :tgl
      t.timestamps
    end
  end
end
