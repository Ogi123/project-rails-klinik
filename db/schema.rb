# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2023_03_08_035941) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "buatjanjis", force: :cascade do |t|
    t.integer "pasien_id"
    t.integer "dokter_id"
    t.integer "poliklinik_id"
    t.integer "jadwalpraktek_id"
    t.date "tgl_janji"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dokters", force: :cascade do |t|
    t.integer "poliklinik_id"
    t.string "nama"
    t.string "email"
    t.string "no_tlp"
    t.integer "jk"
    t.string "alamat"
    t.string "spesialis"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "jadwalprakteks", force: :cascade do |t|
    t.integer "dokter_id"
    t.date "tgl"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medicalrecords", force: :cascade do |t|
    t.integer "pasien_id"
    t.integer "dokter_id"
    t.integer "buatjanji_id"
    t.date "tgl_record"
    t.string "diagnosa"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pasiens", force: :cascade do |t|
    t.string "nama"
    t.string "no_tlp"
    t.string "alamat"
    t.date "tgl_lahir"
    t.integer "jk"
    t.string "tinggi_badan"
    t.string "berat_badan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "polikliniks", force: :cascade do |t|
    t.string "nama_poli"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
