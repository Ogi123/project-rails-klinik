Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      resources :dokters
      resources :polikliniks
      resources :jadwalprakteks
      resources :pasiens
      resources :buatjanjis
      resources :medicalrecords
      post "/login", to: "authenticate#authenticate_dokter"
      resources :posts
    end
  end
end
