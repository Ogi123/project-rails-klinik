class Buatjanji < ApplicationRecord
    belongs_to :pasien
    belongs_to :jadwalpraktek
    has_one    :medicalrecord


    validates   :pasien_id, presence: true
    validates   :dokter_id, presence: true
    validates   :poliklinik_id, presence: true
    validates   :jadwalpraktek_id, presence: true
    validates   :tgl_janji, presence: true

    def new_attributes
        {
          id: self.id,
          pasien_id: self.pasien_id,
          dokter_id: self.dokter_id,
          poliklinik_id: self.poliklinik_id,
          jadwalpraktek_id: self.jadwalpraktek_id,
          tgl_janji: self.tgl_janji,
          created_at: self.created_at,
        }
      end
end
