class Jadwalpraktek < ApplicationRecord

    belongs_to :dokter
    has_one :buatjanji

    validates :dokter_id, presence: true
    validates :tgl, presence: true

    
    def new_attributes
        {
          id: self.id,
          dokter_id: self.dokter_id,
          tgl: self.tgl,
          created_at: self.created_at,
        }
      end
end

