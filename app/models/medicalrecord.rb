class Medicalrecord < ApplicationRecord

    belongs_to :buatjanji

    validates   :pasien_id, presence: true
    validates   :dokter_id, presence: true
    validates   :buatjanji_id, presence:true
    validates   :tgl_record, presence: true
    validates   :diagnosa, presence: true

    def new_attributes
        {
          id: self.id,
          pasien_id: self.pasien_id,
          dokter_id: self.dokter_id,
          buatjanji_id: self.buatjanji_id,
          tgl_record: self.tgl_record,
          diagnosa: self.diagnosa,
          created_at: self.created_at,
        }
    end
end

