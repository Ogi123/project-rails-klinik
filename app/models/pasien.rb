class Pasien < ApplicationRecord

    has_many :buatjanjis, dependent: :destroy

    validates   :nama, presence: true, length: { maximum: 50}
    validates   :no_tlp, presence: true, length: { maximum: 15}
    validates   :alamat, presence: true, length: { maximum: 500}
    validates   :tgl_lahir, presence: true
    validates   :jk, presence: true
    validates   :tinggi_badan, presence: true
    validates   :berat_badan, presence: true

    enum jk: {
        lakilaki: 1,
        perempuan: 2
    }
  
    
    def new_attributes
      {
        id: self.id,
        nama: self.nama,
        no_tlp: self.no_tlp,
        alamat: self.alamat,
        tgl_lahir: self.tgl_lahir,
        jk: self.jk,
        tinggi_badan: self.tinggi_badan,
        berat_badan: self.berat_badan,
        created_at: self.created_at,
      }
    end
end


