class Dokter < ApplicationRecord
    has_secure_password

    belongs_to :poliklinik
    
    has_many :jadwalprakteks, dependent: :destroy

     validates    :poliklinik_id, presence: true, length: { maximum: 50 }
     validates   :nama, presence: true, length: { maximum: 50 }
     validates   :email, presence: true, length: { maximum: 50 }
     validates   :no_tlp, presence: true, length: {maximum: 15 }
     validates   :jk, presence: true
     validates   :alamat, presence: true, length: { maximum: 500 }
     validates   :spesialis, presence: true, length: { maximum: 20 }

    enum jk: {
        lakilaki: 1,
        perempuan: 2

    }

    def new_attributes
      {
        id: self.id,
        poliklinik_id: self.poliklinik_id,
        nama: self.nama,
        email: self.email,
        no_tlp: self.no_tlp,
        jk: self.jk,
        alamat: self.alamat,
        spesialis: self.spesialis,
        created_at: self.created_at,
      }

    end
end
