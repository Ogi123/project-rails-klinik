class Poliklinik < ApplicationRecord
    has_many :dokters, dependent: :destroy

    validates :nama_poli, presence: true, length: { maximum: 25}

    def new_attributes
        {
          id: self.id,
          nama_poli: self.nama_poli,
          created_at: self.created_at,
        }
      end
end

