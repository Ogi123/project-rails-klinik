class Api::V1::PasiensController < ApplicationController
    before_action :set_pasien, only: [:show, :update, :destroy]

    # GET /pasiens
    def index
      @pasiens = Pasien.all
  
      render json: @pasiens.map { |pasien| pasien.new_attributes }
    end
  
    # GET /pasiens/1
    def show
      render json: @pasien.new_attributes
    end
  
    # POST /pasiens
    def create
      @pasien = Pasien.new(pasien_params)
  
      if @pasien.save
        render json: @pasien.new_attributes, status: :created
      else
        render json: @pasien.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /pasiens/1
    def update
      if @pasien.update(pasien_params)
        render json: @pasien.new_attributes
      else
        render json: @pasien.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /pasiens/1
    def destroy
      @pasien.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_pasien
        @pasien = Pasien.find_by_id(params[:id])
        if @pasien.nil?
          render json: { error: "Pasien not found" }, status: :not_found
        end
      end
  
      # Only allow a trusted parameter "white list" through.
      def pasien_params
        params.require(:pasien).permit(:nama, :no_tlp, :alamat, :tgl_lahir, :jk, :tinggi_badan, :berat_badan)
      end
  end
  

