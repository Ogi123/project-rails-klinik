class Api::V1::MedicalrecordsController < ApplicationController
    before_action :set_medicalrecord, only: [:show, :update, :destroy]

    # GET /medicalrecords
    def index
      @medicalrecords = Medicalrecord .all
  
      render json: @medicalrecords.map { |medicalrecord| medicalrecord.new_attributes }
    end
  
    # GET /medicalrecords/1
    def show
      render json: @medicalrecord.new_attributes
    end
  
    # POST /medicalrecords
    def create
      @medicalrecord = Medicalrecord .new(medicalrecord_params)
  
      if @medicalrecord.save
        render json: @medicalrecord.new_attributes, status: :created
      else
        render json: @medicalrecord.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /medicalrecords/1
    def update
      if @medicalrecord.update(medicalrecord_params)
        render json: @medicalrecord.new_attributes
      else
        render json: @medicalrecord.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /medicalrecords/1
    def destroy
      @medicalrecord.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_medicalrecord
        @medicalrecord = Medicalrecord .find_by_id(params[:id])
        if @medicalrecord.nil?
          render json: { error: "Medicalrecord not found" }, status: :not_found
        end
      end
  
      # Only allow a trusted parameter "white list" through.
      def medicalrecord_params
        params.require(:medicalrecord).permit(:pasien_id, :dokter_id, :buatjanji_id, :tgl_record, :diagnosa)
      end
  end
