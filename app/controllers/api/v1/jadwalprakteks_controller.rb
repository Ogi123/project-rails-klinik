class Api::V1::JadwalprakteksController < ApplicationController
    before_action :set_jadwalpraktek, only: [:show, :update, :destroy]

    # GET /jadwalprakteks
    def index
      @jadwalprakteks = Jadwalpraktek.all
  
      render json: @jadwalprakteks.map { |jadwalpraktek| jadwalpraktek.new_attributes }
    end
  
    # GET /jadwalprakteks/1
    def show
      render json: @jadwalpraktek.new_attributes
    end
  
    # POST /jadwalprakteks
    def create
      @jadwalpraktek = Jadwalpraktek.new(jadwalpraktek_params)
  
      if @jadwalpraktek.save
        render json: @jadwalpraktek.new_attributes, status: :created
      else
        render json: @jadwalpraktek.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /jadwalprakteks/1
    def update
      if @jadwalpraktek.update(jadwalpraktek_params)
        render json: @jadwalpraktek.new_attributes
      else
        render json: @jadwalpraktek.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /jadwalprakteks/1
    def destroy
      @jadwalpraktek.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_jadwalpraktek
        @jadwalpraktek = Jadwalpraktek.find_by_id(params[:id])
        if @jadwalpraktek.nil?
          render json: { error: "Jadwalpraktek not found" }, status: :not_found
        end
      end
  
      # Only allow a trusted parameter "white list" through.
      def jadwalpraktek_params
        params.require(:jadwalpraktek).permit(:dokter_id, :tgl)
      end
  end