class Api::V1::DoktersController < ApplicationController
    # skip_before_action :authenticate_request, only: [:create, login]
    # before_action :set_dokter, only: [:show, :update, :destroy]
    #  before_action   :authenticate_request!, except :login
  
    # GET /dokters
    def index
      @dokters = Dokter.all
  
      render json: @dokters.map { |dokter| dokter.new_attributes }
    end
  
    # GET /dokters/1
    def show
      render json: @dokter.new_attributes
    end
  
    # POST /dokters
    def create
      @dokter = Dokter.new(dokter_params)
  
      if @dokter.save
        render json: @dokter.new_attributes, status: :created
      else
        render json: @dokter.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /dokters/1
    def update
      if @dokter.update(dokter_params)
        render json: @dokter.new_attributes
      else
        render json: @dokter.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /dokters/1
    def destroy
      @dokter.destroy
    end
  
    def login
      @dokter = Dokter.find_by(username: params[:username])
      if @dokter && @dokter&.authenticate(params[:password])
        token = JsonWebToken.encode(dokter_id: @dokter.id)
        render json: {
          dokter: @dokter.new_attributes,
          token: token,
        }
      else
        render json: { error: "Invalid username or password" }, status: :unauthorized
      end
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
    def set_dokter
      @dokter = Dokter.find_by_id(params[:id])
      if @dokter.nil?
        render json: { error: "Dokter not found" }, status: :not_found
      end
    end
  
    def dokter_params
      {
        poliklinik_id: params[:poliklinik_id],
        nama: params[:nama],
        email: params[:email],
        no_tlp: params[:no_tlp],
        jk: params[:jk],
        alamat: params[:alamat],
        spesialis: params[:spesialis],
        password: params[:password],
      }
    end
  end
  
