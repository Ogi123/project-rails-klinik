class Api::V1::PolikliniksController < ApplicationController
    before_action :set_poliklinik, only: [:show, :update, :destroy]

    # GET /polikliniks
    def index
      @polikliniks = Poliklinik.all
  
      render json: @polikliniks.map { |poliklinik| poliklinik.new_attributes }
    end
  
    # GET /polikliniks/1
    def show
      render json: @poliklinik.new_attributes
    end
  
    # POST /polikliniks
    def create
      @poliklinik = Poliklinik.new(poliklinik_params)
  
      if @poliklinik.save
        render json: @poliklinik.new_attributes, status: :created
      else
        render json: @poliklinik.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /polikliniks/1
    def update
      if @poliklinik.update(poliklinik_params)
        render json: @poliklinik.new_attributes
      else
        render json: @poliklinik.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /polikliniks/1
    def destroy
      @poliklinik.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_poliklinik
        @poliklinik = Poliklinik.find_by_id(params[:id])
        if @poliklinik.nil?
          render json: { error: "Poliklinik not found" }, status: :not_found
        end
      end
  
      # Only allow a trusted parameter "white list" through.
      def poliklinik_params
        params.require(:poliklinik).permit(:nama_poli)
      end
  end
  
