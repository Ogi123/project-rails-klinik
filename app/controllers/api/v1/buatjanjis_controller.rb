class Api::V1::BuatjanjisController < ApplicationController
    before_action :set_buatjanji, only: [:show, :update, :destroy]

    # GET /buatjanjis
    def index
      @buatjanjis = Buatjanji.all
  
      render json: @buatjanjis.map { |buatjanji| buatjanji.new_attributes }
    end
  
    # GET /buatjanjis/1
    def show
      render json: @buatjanji.new_attributes
    end
  
    # POST /buatjanjis
    def create
      @buatjanji = Buatjanji.new(buatjanji_params)
  
      if @buatjanji.save
        render json: @buatjanji.new_attributes, status: :created
      else
        render json: @buatjanji.errors, status: :unprocessable_entity
      end
    end
  
    # PATCH/PUT /buatjanjis/1
    def update
      if @buatjanji.update(buatjanji_params)
        render json: @buatjanji.new_attributes
      else
        render json: @buatjanji.errors, status: :unprocessable_entity
      end
    end
  
    # DELETE /buatjanjis/1
    def destroy
      @buatjanji.destroy
    end
  
    private
      # Use callbacks to share common setup or constraints between actions.
      def set_buatjanji
        @buatjanji = Buatjanji.find_by_id(params[:id])
        if @buatjanji.nil?
          render json: { error: "Buatjanji not found" }, status: :not_found
        end
      end
  
      # Only allow a trusted parameter "white list" through.
      def buatjanji_params
        params.require(:buatjanji).permit(:pasien_id, :dokter_id, :poliklinik_id, :jadwalpraktek_id, :tgl_janji)
      end
  end
